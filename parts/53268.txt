General
	Part Number	Magnetorquer
	Part Name	Magnetorquer
	Description
	Material Name	Steel

Manage
	Item Number
	Lifecycle
	Revision
	State	Working
	Change Order

Physical
	Mass	58.053 g
	Volume	7395.244 mm^3
	Density	0.008 g / mm^3
	Area	2990.011 mm^2
	World X,Y,Z	-34.50 mm, 46.827 mm, -26.00 mm
	Center of Mass	-34.50 mm, 48.078 mm, -26.00 mm
	Bounding Box
		Length	11.00 mm
		Width	84.00 mm
		Height	11.00 mm
	Moment of Inertia at Center of Mass   (g mm^2)
		Ixx	29861.282
		Ixy	0.00
		Ixz	0.00
		Iyx	0.00
		Iyy	865.842
		Iyz	-1.175E-06
		Izx	0.00
		Izy	-1.175E-06
		Izz	29861.282
	Moment of Inertia at Origin   (g mm^2)
		Ixx	2.033E+05
		Ixy	96290.559
		Ixz	-52073.238
		Iyx	96290.559
		Iyy	1.092E+05
		Iyz	72566.798
		Izx	-52073.238
		Izy	72566.798
		Izz	2.331E+05

