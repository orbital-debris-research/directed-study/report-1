General
	Part Number	central_frame
	Part Name	central_frame
	Description
	Material Name	SOLIDWORKS Materials|Plain Carbon Steel

Manage
	Item Number
	Lifecycle
	Revision
	State	Working
	Change Order

Physical
	Mass	0.00 g
	Volume	12245.003 mm^3
	Density	0.00 g / mm^3
	Area	18268.074 mm^2
	World X,Y,Z	0.00 mm, 58.689 mm, 0.00 mm
	Center of Mass	0.00 mm, 0.00 mm, 0.00 mm
	Bounding Box
		Length	51.213 mm
		Width	89.465 mm
		Height	51.213 mm
	Moment of Inertia at Center of Mass   (g mm^2)
		Ixx	0.00
		Ixy	0.00
		Ixz	0.00
		Iyx	0.00
		Iyy	0.00
		Iyz	0.00
		Izx	0.00
		Izy	0.00
		Izz	0.00
	Moment of Inertia at Origin   (g mm^2)
		Ixx	0.00
		Ixy	0.00
		Ixz	0.00
		Iyx	0.00
		Iyy	0.00
		Iyz	0.00
		Izx	0.00
		Izy	0.00
		Izz	0.00

